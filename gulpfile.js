var gulp         = require('gulp');
var browser      = require('browser-sync');
var plumber      = require("gulp-plumber");
var watch        = require("gulp-watch");
var ejs          = require('gulp-ejs');
var rename       = require("gulp-rename");
var fs           = require('fs');

var sass         = require('gulp-sass');
var sourcemaps   = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var minifyCss    = require('gulp-minify-css');
var changed      = require('gulp-changed');

var gutil        = require('gulp-util');
// var ftp          = require('gulp-ftp');
var sftp         = require('gulp-sftp');

var gifsicle     = require('imagemin-gifsicle');
var jpegtran     = require('imagemin-jpegtran');
var optipng      = require('imagemin-optipng');

var merge = require('event-stream').merge;


gulp.task('server', function() {
    browser({server: {
        baseDir: 'dest'
    }});
});

gulp.task('upload', function (){
    var json = JSON.parse(fs.readFileSync("./server.json"));
    return gulp.src(['dest/**/*'])
               .pipe(sftp({
                    host       : json.host,
                    user       : json.user,
                    pass       : json.pass,
                    remotePath : json.remotePath
               })).pipe(gutil.noop());
});
gulp.task('ftp', ['upload'],function(){
    console.log('↓TEST-UP URL↓\u001b[34m');
    console.log('https://life-escort.two-five.info/');
    console.log('\u001b[0m↑TEST-UP URL↑');
});



gulp.task('css', function(){
    return gulp.src('dev/scss/*.scss')
            .pipe(plumber({
              errorHandler: function (error) {
                console.log(error.message);
                this.emit('end');
            }}))
            .pipe(changed('dest/css'))
            .pipe(sourcemaps.init())
            .pipe(sass())
            .pipe(autoprefixer({
                browsers: ["last 2 versions", "ie >= 9", "Android >= 4","ios_saf >= 8"],
                cascade: false
            }))
            .pipe(minifyCss({advanced:false})) // minify
            .pipe(sourcemaps.write('./'))
            .pipe(gulp.dest('dest/css'));
});


gulp.task('js', function(){
    gulp.src('dev/js/*.js')
               .pipe( gulp.dest('dest/js') );
    gulp.src('dev/include/*.js')
               .pipe( gulp.dest('dest/include') );
});

gulp.task('html', function(){
    var main = gulp.src('dev/pages/*.ejs')
        .pipe(plumber())
        .pipe(ejs())
        .pipe( rename({
            extname: '.html'
        }))
        .pipe( gulp.dest('dest') );
    var inquiry = gulp.src('dev/inquiry/*.ejs')
        .pipe(plumber())
        .pipe(ejs())
        .pipe( rename({
            extname: '.html'
        }))
        .pipe( gulp.dest('dest/prg_include/view/_html/form/_inc') );
    var inquiry_template = gulp.src('dev/inquiry/html_template/**/*')
        .pipe(plumber())
        .pipe( gulp.dest('dest/prg_include/view/_html/form/') );
  return merge(main,inquiry,inquiry_template);
});

gulp.task('php', function() {
    gulp.src('dev/include/footer_main-code.ejs')
        .pipe(plumber())
        .pipe(ejs())
        .pipe( rename({
            basename: 'footer',
            extname: '.php'
        }))
        .pipe( gulp.dest('dest/include') );
    gulp.src('dev/include/header_main-code.ejs')
        .pipe(plumber())
        .pipe(ejs({data:{pageType:""}}))
        .pipe( rename({
            basename: 'header',
            extname: '.php'
        }))
        .pipe( gulp.dest('dest/include') );
});

gulp.task('img', function() {
  var jpg = gulp.src(['dev/img/*.jpg'])
             .pipe(plumber())
             .pipe(changed('dest/img'))
             .pipe(jpegtran()())
             .pipe(gulp.dest('dest/img'));
  var png = gulp.src(['dev/img/*.png'])
             .pipe(plumber())
             .pipe(changed('dest/img'))
             .pipe(optipng()())
             .pipe(gulp.dest('dest/img'));
  var gif = gulp.src(['dev/img/*.gif'])
             .pipe(plumber())
             .pipe(changed('dest/img'))
             .pipe(gifsicle()())
             .pipe(gulp.dest('dest/img'));
  return merge(jpg,png,gif);
});



gulp.task('watch', function() {
    watch(['dev/scss/*.scss','dev/img/*','dev/**/*.ejs','dev/**/*.js'], function(event){gulp.start("build");});
});

gulp.task('build', ['css','img','js','html','php'],function(){
    browser.reload();
});

gulp.task('default', ['build','watch','server']);
