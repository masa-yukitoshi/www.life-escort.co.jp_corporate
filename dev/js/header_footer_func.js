$(function(){
	var isSmartPhone = false;
	if($("#contentsbody")[0]){
		var minSize = 767;//ルート 静的ページ
	}else{
		var minSize = 640;//ec-support wpディレクトリ
	}
	if($(window).width()<=minSize){
		isSmartPhone = true;
	}else{
		isSmartPhone = false;
	}
	$(window).resize(function(){
		if($(this).width()<=minSize){
			isSmartPhone = true;
		}else{
			isSmartPhone = false;
		}
	});

	$("#menu-trigger").click(function(){
		$(this).toggleClass('active');
		$("body").toggleClass('menu-open');
		$("#site-header").find(".sp-menu-wrapper").toggleClass('active');
		return false;
	});

	$("#header_nav>ul").children("li").hover(function(){
		if(!isSmartPhone){$(this).find(".sub").not(":animated").slideDown();}
	},function(){
		if(!isSmartPhone){$(this).find(".sub").slideUp();}
	});


	$("#topPageLogo").addClass("on");
	setTimeout(function(){
		$("#topPageLogo").find(".noface").hide();
		$("#topPageLogo").find(".onlyface").hide();
		$("#topPageLogo").find(".animate").show();
	},1200);
	var srcPath = $("#topPageLogo").find(".animate").attr("src");
	var srcPath2 = srcPath.replace(".gif", "2.gif");
	$("#topPageLogo").mouseover(function(){
		$(this).find(".animate").attr("src",srcPath2);
		setTimeout(function(){
			$("#topPageLogo").find(".animate").attr("src",srcPath);
		},100);
	});

});
